import React, { Component } from 'react';
import  { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import { Navbar } from 'react-bootstrap';
import { Nav, NavItem} from 'react-bootstrap';
import './App.css';
import Routes from './Routes';
import logo from './logo.svg';

class App extends Component {
    render() {
        return (
            <div className='App container'>
            <Navbar fluid collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                          <Link to="/"><img src={logo} style={{height: 30, width: 100, marginTop: -5}} className="App-logo" alt=""/></Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                      <Nav pullRight>
                        <LinkContainer to='/login'>
                          <NavItem>Login</NavItem>
                        </LinkContainer>
                      </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Routes />
                </div>
        );
    }
}
export default App;
