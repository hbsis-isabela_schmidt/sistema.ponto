import React from 'react';
import { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import './Login.css';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: '',
            senha: '',
            token: ''
        };
    }

    onClickSubmitBtn() {
        this.onFetchLogin();
    }

    validacao() {
        return this.state.user.length > 0 && this.state.senha.length > 0
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
    }

    render() {
        return (
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="user" bsSize="sm">
                        <ControlLabel>Usuário</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            autoComplete="username"
                            value={this.state.user}
                            onChange={this.handleChange} />
                    </FormGroup>
                    <FormGroup controlId="senha" bsSize="sm">
                        <ControlLabel>Senha</ControlLabel>
                        <FormControl
                            autoFocus
                            type="password"
                            autoComplete="current-password"
                            value={this.state.senha}
                            onChange={this.handleChange} />
                    </FormGroup>

                    <Button
                        block
                        bsSize="sm"
                        disabled={!this.validacao()}
                        type="submit"
                        onClick={this.onClickSubmitBtn.bind(this)}
                    >Entrar</Button>
                </form>
            </div>
        )
    }


    async onFetchLogin() {
        

        try {
            let response = await fetch(
                "http://hb-vw12senior02/rondaweb/conector?ACAO=EXESENHA&SIS=HR&RANDOM=183146616",

                {
                    method: "POST",
                    headers: {
                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                        "Accept-Encoding": "gzip, deflate",
                        "Accept-Language": "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7",
                        "Cache-Control": "max-age=0",
                        "Connection": "keep-alive",
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Host": "hb-vw12senior02",
                        "Origin": "http://hb-vw12senior02",
                        "Referer": "http://hb-vw12senior02/rondaweb/",
                        "Upgrade-Insecure-Requests": "1",
                        "Access-Control-Allow-Origin": "localhost:3000/",
                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"
                    },
                    body: {

                        "STATUS": "Enviar",
                        "DesId": "",
                        "HtmlAtu": "hrpsenha.htm",
                        "PrxPag": "hrgeral.htm",
                        "NavigatorInitInfo": "Chrome",
                        "SelLang": "",
                        "LANGUAGESELECTED": "",
                        "NomUsu": this.state.user,
                        "SenUsu": this.state.senha,
                    },
                }
            );

            
            return console.log(response);
        }

        catch (errors) {
            console.log("erro");

        }
        
    }
}